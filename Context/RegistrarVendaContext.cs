using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Models;



namespace TechTestPaymentApi.Context
{
    public class RegistrarVendaContext : DbContext
    {
        public RegistrarVendaContext(DbContextOptions<RegistrarVendaContext> options) : base (options)
        {

        }
        public DbSet<Venda> Vendas { get; set; }
    }
    
}