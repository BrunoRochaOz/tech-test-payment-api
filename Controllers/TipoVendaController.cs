using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TechTestPaymentApi.Models;
using TechTestPaymentApi.Context;


namespace TechTestPaymentApi.Controllers
{
    public class TipoVenda
    {
        [ApiController]
        [Route("[controller]")]
        public class TipoVendaController : ControllerBase
        {
            private readonly RegistrarVendaContext _context;

            public TipoVendaController(RegistrarVendaContext context)
            {
                _context = context;
            }


            [HttpPost ("Venda")]
            public IActionResult CompraRealizada(Venda venda)
            {
                _context.Add(venda);
                _context.SaveChanges();
                return CreatedAtAction(nameof(ObterPorId), new { id = venda }, venda);
            }


            [HttpGet("{id}")]
            public IActionResult ObterPorId(int id)
            {
                var venda = _context.Vendas.Find(id);

                if (venda == null)
                    return NotFound();

                return Ok(venda);
            }


            [HttpGet("ObterPorStatus")]
            public IActionResult ObterPorStatus(EnumStatusVenda status)
            {
                var listaVendas = _context.Vendas.Where(x => x.Status == status);

                var AguardandoPagamento = _context.Vendas.Find(listaVendas);
                var PagamentoAprovado = _context.Vendas.Find(listaVendas);                
                var EnviadoParaTransportadora = _context.Vendas.Find(listaVendas);
                var Entregue = _context.Vendas.Find(listaVendas);

                if (Entregue == null)
                    return Ok(EnviadoParaTransportadora);
                
                if (EnviadoParaTransportadora == null)
                    return Ok (PagamentoAprovado);

                if (PagamentoAprovado == null)
                    return Ok(AguardandoPagamento);

                if (AguardandoPagamento == null)
                    return Ok(Entregue);
                
                _context.SaveChanges(); 
                return Ok(listaVendas);
            }

            [HttpDelete("{id}")]
            public IActionResult Cancelar (int id)
            {
                var CompraRealizada = _context.Vendas.Find(id);

                if(CompraRealizada == null)
                    return NotFound();

                _context.Vendas.Remove(CompraRealizada);
                Console.WriteLine("Compra Cancelada com Sucesso!!!");
                
                _context.SaveChanges();                
                return NoContent();
            }          


            //Implementa o valor atualizar na pesquisa de vendas 

            [HttpPut ("{id}")]
            public IActionResult Atualizar (int id, Venda venda)
            {
                var vendasRealizadas = _context.Vendas.Find(id);
                
                if (vendasRealizadas == null)
                    return NotFound();

                _context.Vendas.Update(venda);
                _context.SaveChanges();

                return Ok(vendasRealizadas);
               
            }
                        
        }

    }
}