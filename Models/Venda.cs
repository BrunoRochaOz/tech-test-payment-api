using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentApi.Models
{
    public class Venda
    {
        //id, cpf, nome, e-mail e telefone
        public int Id { get; set; }
        public int Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public int Telefone { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}